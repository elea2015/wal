
(function($) {
    $(document).ready(function() {
        var newVal = 'Visit module';
        $('.more-link').html( newVal );

        //change tooltop text
        $('.ld-status-unlocked').attr('data-ld-tooltip','Unlocked')
        $('.single-sfwd-courses .ld-status-unlocked').text('Unlocked');

        //Add class to enrolled courses
		$('.ribbon-enrolled').parent().addClass("enrolled");
		$('.ld_course_grid_price').parent().addClass("enrolled");

        //add id to learndash container
        $('.learndash-wrapper').attr('id', 'page-container');
    });

})(jQuery);