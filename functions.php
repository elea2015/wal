<?php
function my_theme_enqueue_styles() { 
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

// END ENQUEUE PARENT ACTION

//Show memberiun prohibit content
function memberium_show(){
	echo do_shortcode('[memb_set_prohibited_action action=show]');
}

//add_action('wp_head', 'memberium_show');

function memberium_update() {
	echo do_shortcode('[memb_sync_contact]');
}

add_action('wp_head', 'memberium_update');

/**
 *	This will hide the Divi "Project" post type.
 *	Thanks to georgiee (https://gist.github.com/EngageWP/062edef103469b1177bc#gistcomment-1801080) for his improved solution.
 */
add_filter( 'et_project_posttype_args', 'mytheme_et_project_posttype_args', 10, 1 );
function mytheme_et_project_posttype_args( $args ) {
	return array_merge( $args, array(
		'public'              => false,
		'exclude_from_search' => false,
		'publicly_queryable'  => false,
		'show_in_nav_menus'   => false,
		'show_ui'             => false
	));
}


// Register Custom Post Type Module
function create_module_cpt() {

	$labels = array(
		'name' => _x( 'Modules', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Module', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Modules', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Module', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Module Archives', 'textdomain' ),
		'attributes' => __( 'Module Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Module:', 'textdomain' ),
		'all_items' => __( 'All Modules', 'textdomain' ),
		'add_new_item' => __( 'Add New Module', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Module', 'textdomain' ),
		'edit_item' => __( 'Edit Module', 'textdomain' ),
		'update_item' => __( 'Update Module', 'textdomain' ),
		'view_item' => __( 'View Module', 'textdomain' ),
		'view_items' => __( 'View Modules', 'textdomain' ),
		'search_items' => __( 'Search Module', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Module', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Module', 'textdomain' ),
		'items_list' => __( 'Modules list', 'textdomain' ),
		'items_list_navigation' => __( 'Modules list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Modules list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Module', 'textdomain' ),
		'description' => __( 'Modules', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-layout',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'comments', 'page-attributes', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'module', $args );

}
add_action( 'init', 'create_module_cpt', 0 );

//grey out access to courses

function memberium_grey_no_access(){
	echo "
		<script>
			function myFunction(module) {
				var element = document.getElementById(module);
				element.classList.add('no-access');
			}
		</script>";
	$usertags = array(2041,2043,2045,2047,2049,2051);
	$modules = array("post-59","post-234","post-242","post-244","post-246","post-249");
	
	$i = 0;
	foreach ($usertags as $usertag){
		$tag = memb_hasAllTags( $usertag, $contact_id = false );
		if($usertag == 2041 and $tag == FALSE){
			echo "<script>myFunction('post-486')</script>";
		}elseif($usertag == 2043 and $tag == FALSE){
			echo "<script>myFunction('post-534')</script>";
		}elseif($usertag == 2045 and $tag == FALSE){
			echo "<script>myFunction('post-536')</script>";
		}elseif($usertag == 2047 and $tag == FALSE){
			echo "<script>myFunction('post-538')</script>";
		}elseif($usertag == 2049 and $tag == FALSE){
			echo "<script>myFunction('post-540')</script>";
		}elseif($usertag == 2051 and $tag == FALSE){
			echo "<script>myFunction('post-542')</script>";
		}		
	}
	
	
	// // echo "
	// // 	<script>
	// // 		function myFunction(module) {
	// // 			var element = document.getElementById(module);
	// // 			element.classList.add('no-access');
	// // 		}
	// // 	</script>
	// // ";
	// foreach ($usertags as $usertag){
	// 	$tag = memb_hasAllTags( $usertag, $contact_id = false );
	// 	if($tag == FALSE and $usertag == 241){
	// 		echo "<script>myFunction(module)</script>"
	// 	}
	// }
}

//add_action('wp_footer' , 'memberium_grey_no_access');


// Register Custom Post Type Video
function create_resources_cpt() {

	$labels = array(
		'name' => _x( 'Resources', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Resource', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Resources', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Resource', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Resource Archives', 'textdomain' ),
		'attributes' => __( 'Resource Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Resource:', 'textdomain' ),
		'all_items' => __( 'All Resources', 'textdomain' ),
		'add_new_item' => __( 'Add New Resource', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Resource', 'textdomain' ),
		'edit_item' => __( 'Edit Resource', 'textdomain' ),
		'update_item' => __( 'Update Resource', 'textdomain' ),
		'view_item' => __( 'View Resource', 'textdomain' ),
		'view_items' => __( 'View Resources', 'textdomain' ),
		'search_items' => __( 'Search Resource', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Resource', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Resource', 'textdomain' ),
		'items_list' => __( 'Resources list', 'textdomain' ),
		'items_list_navigation' => __( 'Resources list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Resources list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Resources', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-video-alt3',
		'supports' => array('title', 'editor', 'excerpt', 'thumbnail', 'revisions', 'author', 'page-attributes', 'post-formats', 'custom-fields'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'resources', $args );

}
add_action( 'init', 'create_resources_cpt', 0 );

// Register Custom Post Type Exercise
function create_exercise_cpt() {

	$labels = array(
		'name' => _x( 'Exercises', 'Post Type General Name', 'textdomain' ),
		'singular_name' => _x( 'Exercise', 'Post Type Singular Name', 'textdomain' ),
		'menu_name' => _x( 'Exercises', 'Admin Menu text', 'textdomain' ),
		'name_admin_bar' => _x( 'Exercise', 'Add New on Toolbar', 'textdomain' ),
		'archives' => __( 'Exercise Archives', 'textdomain' ),
		'attributes' => __( 'Exercise Attributes', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Exercise:', 'textdomain' ),
		'all_items' => __( 'All Exercises', 'textdomain' ),
		'add_new_item' => __( 'Add New Exercise', 'textdomain' ),
		'add_new' => __( 'Add New', 'textdomain' ),
		'new_item' => __( 'New Exercise', 'textdomain' ),
		'edit_item' => __( 'Edit Exercise', 'textdomain' ),
		'update_item' => __( 'Update Exercise', 'textdomain' ),
		'view_item' => __( 'View Exercise', 'textdomain' ),
		'view_items' => __( 'View Exercises', 'textdomain' ),
		'search_items' => __( 'Search Exercise', 'textdomain' ),
		'not_found' => __( 'Not found', 'textdomain' ),
		'not_found_in_trash' => __( 'Not found in Trash', 'textdomain' ),
		'featured_image' => __( 'Featured Image', 'textdomain' ),
		'set_featured_image' => __( 'Set featured image', 'textdomain' ),
		'remove_featured_image' => __( 'Remove featured image', 'textdomain' ),
		'use_featured_image' => __( 'Use as featured image', 'textdomain' ),
		'insert_into_item' => __( 'Insert into Exercise', 'textdomain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this Exercise', 'textdomain' ),
		'items_list' => __( 'Exercises list', 'textdomain' ),
		'items_list_navigation' => __( 'Exercises list navigation', 'textdomain' ),
		'filter_items_list' => __( 'Filter Exercises list', 'textdomain' ),
	);
	$args = array(
		'label' => __( 'Exercise', 'textdomain' ),
		'description' => __( '', 'textdomain' ),
		'labels' => $labels,
		'menu_icon' => 'dashicons-list-view',
		'supports' => array('title', 'editor', 'revisions', 'author'),
		'taxonomies' => array(),
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'menu_position' => 5,
		'show_in_admin_bar' => true,
		'show_in_nav_menus' => true,
		'can_export' => true,
		'has_archive' => true,
		'hierarchical' => false,
		'exclude_from_search' => false,
		'show_in_rest' => true,
		'publicly_queryable' => true,
		'capability_type' => 'post',
	);
	register_post_type( 'exercise', $args );

}
add_action( 'init', 'create_exercise_cpt', 0 );


function custom_scripts_js() {
    wp_enqueue_script('scripts', get_stylesheet_directory_uri() . '/js/scripts.js', array('jquery'), null, false); // Here in this example the last value has been set as true, so, it will be loaded in the footer.
}
add_action('wp_enqueue_scripts', 'custom_scripts_js');

// remove wp version number from scripts and styles
function remove_css_js_version( $src ) {
    if( strpos( $src, '?ver=' ) )
        $src = remove_query_arg( 'ver', $src );
    return $src;
}
add_filter( 'style_loader_src', 'remove_css_js_version', 9999 );
add_filter( 'script_loader_src', 'remove_css_js_version', 9999 );